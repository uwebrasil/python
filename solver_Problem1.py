#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import namedtuple
import math
from operator import itemgetter

Point = namedtuple("Point", ['x', 'y'])
Facility = namedtuple("Facility", ['index', 'setup_cost', 'capacity', 'location'])
Customer = namedtuple("Customer", ['index', 'demand', 'location'])

def length(point1, point2):
    return math.sqrt((point1.x - point2.x)**2 + (point1.y - point2.y)**2)

def minCost(customer,facilities):
    m = 1000000
    i = -1
    for f in facilities:
        l =  length(customer.location, f.location)      
        if l < m :
            m = l
            i = f.index    
    return i , m   
    
def solve_it(input_data):
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = input_data.split('\n')

    parts = lines[0].split()
    facility_count = int(parts[0])
    customer_count = int(parts[1])
    
    facilities = []
    for i in range(1, facility_count+1):
        parts = lines[i].split()
        facilities.append(Facility(i-1, float(parts[0]), int(parts[1]), Point(float(parts[2]), float(parts[3])) ))
                
    customers = []
    for i in range(facility_count+1, facility_count+1+customer_count):
        parts = lines[i].split()
        customers.append(Customer(i-1-facility_count, int(parts[0]), Point(float(parts[1]), float(parts[2]))))
  
    facilities = sorted(facilities, key = itemgetter(1))
        
      
    # build a trivial solution
    # pack the facilities one by one until all the customers are served
    solution = [-1]*len(customers)
    capacity_remaining = [f.capacity for f in facilities]

    TOTAL = 0
    facility_index = 0
    for customer in customers:
        if capacity_remaining[facilities[facility_index].index] >= customer.demand:            
            solution[customer.index], X = minCost(customer,facilities)
            TOTAL += X 
            capacity_remaining[facilities[facility_index].index] -= customer.demand
        else:
            facility_index += 1
            assert capacity_remaining[facilities[facility_index].index] >= customer.demand          
            solution[customer.index], X = minCost(customer,facilities)
            TOTAL += X 
            capacity_remaining[facilities[facility_index].index] -= customer.demand      

    
    single = [ x for x in solution if solution.count(x) == 1]
    print "Single", single
    more = set( x for x in solution if solution.count(x) > 1)
    print "more", more
    
    for x in single:
        f = [a for a in facilities if a.index==x]
        if f[0].setup_cost == 0: continue
        #print f
        
        #c = [y for y in customers if y.index==x]
        xx =-1
        for idx,b in enumerate(solution):
            if b == x:
                xx = idx
        c = customers[xx]        
            
        #print c
        cost = length(c.location, f[0].location) 
        print "cost", f[0].index,c.index,cost
        lucro = 0
        swap = [-1,-1]
        for y in more:
            fx = [a for a in facilities if a.index==y]
            new_cost = length(c.location, fx[0].location) 
            print "new_cost", fx[0].index,c.index,new_cost
            
            if (cost+fx[0].setup_cost) - new_cost> 0:
                print "TREFFER", (cost+fx[0].setup_cost) - new_cost                
                print f[0].index, c.index, fx[0].index
                if (cost+fx[0].setup_cost) - new_cost > lucro:
                    lucro = cost+f[0].setup_cost - new_cost 
                    print lucro
                    swap[0] =  c.index
                    swap[1] =  fx[0].index 
        if lucro > 0:
            solution[swap[0]] = swap[1]
            TOTAL -= (lucro - f[0].setup_cost)
            print lucro
    
    used = [0]*len(facilities)
    for facility_index in solution:
        used[facility_index] = 1
        
    # calculate the cost of the solution
    obj = sum([f.setup_cost*used[f.index] for f in facilities])
    obj += TOTAL   

    # prepare the solution in the specified output format
    output_data = str(obj) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))

    return output_data


import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print 'Solving:', file_location
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)'

