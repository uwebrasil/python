#!/usr/bin/python
# -*- coding: utf-8 -*-

import pdb
import math
from collections import namedtuple
from operator    import itemgetter
from copy import deepcopy,copy

from solver_lib import Point, Facility, Customer  
from solver_lib import solution, customers, facilities , TOTAL 
from solver_lib import length #, objective_function 
#from solver_lib import getFacility
#open_facilities = []
#closed_facilities = []
########################################################################
########################################################################
def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    global TOTAL
    global solution
    global facilities
   
    global customers
    # parse the input
    lines = input_data.split('\n')

    parts = lines[0].split()
    facility_count = int(parts[0])
    customer_count = int(parts[1])
    
    for i in range(1, facility_count+1):
        parts = lines[i].split()
        facilities.append(Facility(i-1, float(parts[0]), int(parts[1]), Point(float(parts[2]), float(parts[3])), float(parts[0]) / int(parts[1]) ))
                
    for i in range(facility_count+1, facility_count+1+customer_count):
        parts = lines[i].split()
        customers.append(Customer(i-1-facility_count, int(parts[0]), Point(float(parts[1]), float(parts[2]))))
           
    facilities = sorted(facilities, key = itemgetter(4))
    
    solution = [-1]*len(customers)
    
    used_customer = []
    for f in facilities:
        #print f.index
        used_capacity = 0   
        cl = clist(f,used_customer)  
        cl = sorted(cl, key = itemgetter(1))
        #pdb.set_trace()  
        if len(cl) == 0: break
        index = 0
       
        while True:
           if used_capacity + cl[index][0].demand > f.capacity: break
           used_capacity += cl[index][0].demand
           solution[cl[index][0].index] = f.index 
           TOTAL += cl[index][1]
           used_customer.append(cl[index][0])
           index += 1
           if index >= len(cl): break
   
    
    for cnt in range(5):
       for o in set(solution):
          #cx = c4f(o,True)
          cx = c4f(o,False)
          cx = sorted(cx, key = itemgetter(1), reverse=True)     
          loop = len(cx)
          for tt in range(loop):  
             move(cx[tt])
                  
    for idx,x in enumerate(facilities):
        close_facility(idx)
       
    open_facilities = set(solution)
    closed_facilities = set([x for x in range(len(facilities))]) - set(solution) 
    for idx,i in enumerate(solution):
        c = getCustomer(idx)
        a, b = minCostFORCE(c,closed_facilities)       
        if b < print_facility_customer(solution[idx],c.index,False):
            #print "TREFFER ", i
            #print "idx :",idx, "\t","solution[idx]", solution[idx], "\t",print_facility_customer(solution[idx],c.index,False), "\t", b
            TOTAL -= print_facility_customer(solution[idx],c.index,False)
            TOTAL += b
            solution[idx] = a
   
    overloaded = [f for f in facilities if f.capacity < fillstate(f)]
    
    for f in overloaded:
        #print f.index        
        
        #pdb.set_trace()
        while fillstate(f) > f.capacity:
         #print  fillstate(f)  
         index = -1 
         swap_customer = None
         minc = 1000000
         cc = c4f(f.index,False)
         blacklist = set([x for x in range(len(facilities))]) - set(solution) 
         blacklist.add(f.index)
         for c in cc:
            a, b = minCost(c[0],blacklist)
            oldlength = length(f.location,c[0].location)
            newlength = b
            kosten = newlength - oldlength
            if kosten < minc:
                minc = kosten
                index = a
                swap_customer = c[0]
         #pdb.set_trace()        
         if index >=0:
            solution[swap_customer.index] = index
            TOTAL -= length(f.location,swap_customer.location)
            TOTAL += length(getFacility(index).location,swap_customer.location)
   
    
    
    obj = objective_function()
    # prepare the solution in the specified output format
    output_data = str(obj) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))
        
   
    #pdb.set_trace()     
    return output_data

########################################################################
def move(c):
    global TOTAL
    #global open_facilities
    #global closed_facilities
    global solution
    assert  isinstance(c[0],Customer),"c must be an instance of Customer (1)"  
    open_facilities = set(solution)
    closed_facilities = set([x for x in range(len(facilities))]) - set(solution) 
    #pdb.set_trace()
    #print "Open FacilitiesXX",open_facilities
    #print "Closed FacilitiesXX", closed_facilities
    ## search in open_facilities
    for xx in open_facilities:
        x = getFacility(xx)
        if fillstate(x) + c[0].demand > x.capacity: continue
        if length(x.location,c[0].location) < c[1]:
            #pdb.set_trace()
            solution[c[0].index] = x.index
            TOTAL -= c[1]
            TOTAL += length(x.location,c[0].location)
            return c[1] - length(x.location,c[0].location)
    ## search in closed facilities        
    candidates =[]        
    #pdb.set_trace() 
    for xx in closed_facilities:
        x = getFacility(xx)
        #print "XX",c[0].index, c[1]
        if length(x.location,c[0].location) < c[1]:
            #pdb.set_trace()
            candidates.append( (c,length(x.location,c[0].location),x.index) )
    if len(candidates) == 0: return -1
    #pdb.set_trace() 
    candidates = sorted(candidates,key = itemgetter(1))   
    solution[c[0].index] = candidates[0][2]
    
    TOTAL -= candidates[0][0][1]       
    TOTAL += candidates[0][1]
    return candidates[0][0][1] - candidates[0][1] 
########################################################################
def close_facility(index,force=False):
    global TOTAL
    global solution
    global facilities
    
    solution_save = solution[:]
    before = objective_function()
    TX = copy(TOTAL) 
    
    fac = getFacility(index)
    cl  = c4f(fac.index)
    cval = 0
    
    for x in cl:
        index, value = minCost(x[0],[fac.index]) 
        TOTAL -= x[1]
        TOTAL += value
        #pdb.set_trace()
        solution[x[0].index] = index
    after = objective_function()
    
    if after > before:
        TOTAL = TX
        solution = solution_save
   
########################################################################     
def minCostFORCE(c, blacklist):
    #global solution
    #global customers
    #global facilities
    assert  isinstance(c,Customer),"c must be an instance of Customer (1)"
    m = 1000000    
    i = -1
    for f in facilities:
        if f.index in blacklist: continue
        l =  length(c.location, f.location)   
        
        #if (l < m)  and (fillstate(f) + c.demand <= f.capacity) :   
        if (l < m)  : #and (fillstate(f) + c.demand <= f.capacity) :               
            m = l
            i = f.index    
    return i , m     
########################################################################
def minCost(c, blacklist):
    #global solution
    #global customers
    #global facilities
    assert  isinstance(c,Customer),"c must be an instance of Customer (1)"
    m = 1000000    
    i = -1
    for f in facilities:
        if f.index in blacklist: continue
        l =  length(c.location, f.location)   
        
        if (l < m)  and (fillstate(f) + c.demand <= f.capacity) :   
        #if (l < m)  : #and (fillstate(f) + c.demand <= f.capacity) :               
            m = l
            i = f.index    
    return i , m     
########################################################################         
def clist(f,used_customer):
    cl = []
    for c in customers:
        if c in used_customer: continue
        cl.append( (c,length(f.location,c.location)) )
    cl = sorted(cl, key = itemgetter(1)) 
    return cl
########################################################################
def objective_function():
    obj = 0
    for x in facilities:
        if x.index in solution:
          obj += x.setup_cost      
    obj += TOTAL   
    return obj
########################################################################
def print_facility_customer(findex,cindex,p = True):
    fac = [ x for x in facilities if x.index==findex][0]
    if p:
       print "facility\t",fac.index,"\tcustomer\t",customers[cindex].index, \
       "\tLength: ",length(fac.location,customers[cindex].location)
    return length(fac.location,customers[cindex].location)   
########################################################################
def fillstate(fac):  
       #global solution
       #global customers 
       assert  isinstance(fac,Facility),"fac must be an instance of Facility (1)"   
       s = 0
       for idx,x in enumerate(solution):
           if x == fac.index:
               s += customers[idx].demand
       return s       
########################################################################
def getFacility(findex):
    return [ x for x in facilities if x.index==findex][0]
########################################################################
def getCustomer(cindex):
    return [ x for x in customers if x.index==cindex][0]
########################################################################     
def c4f(findex, p = False):
    fac = [ x for x in facilities if x.index==findex][0]
    assert  isinstance(fac,Facility),"fac must be an instance of Facility (2)" 
    if p:
       print "Index:\t",findex,"\tcapacity:\t",fac.capacity, "\tfillstate ",fillstate(fac),"\tsetup_cost",fac.setup_cost
    cl = []   
    for idx,x in enumerate(solution):
        if x == findex:        
            cl.append( (customers[idx],length(fac.location,customers[idx].location)) )
            if p: 
                print "\tcustomer: ",idx,"\tlength: ",length(fac.location,customers[idx].location), \
                "demand: ",customers[idx].demand        
    return cl            
########################################################################
import sys
if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print 'Solving:', file_location
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)'
########################################################################
