# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 06:55:49 2013

@author: uwe
PROBLEM 4 - wl_100_4
"""
from time import time
from numpy import array, swapaxes
from operator import itemgetter
from types import *
from copy import deepcopy
from random import shuffle

num_warehouses = 0
num_customers = 0
Plants = None
supplyData = None
Stores =None
demand = None
costs = None

WAREHOUSES = { }
TOTAL_COST = 0
open_warehouses = []
closed_ones = []
customer_warehouse = { }
#avg = 0

def warehouse_with_highest_demand():
    return max(supplyData, key=supplyData.get)

def get_next_costumer(warehouse,blacklist):

    minval = 1000000000.0
    index  = -1
    for idx, cost in enumerate(costs[int(warehouse)]):
        if idx in blacklist: continue
        if cost < minval:
            minval = cost
            index  = idx
    return index

def compare_entries(old_warehouse, new_warehouse,message = True):
    assert type(old_warehouse) is IntType, "old_warehouse is not an integer: %r" % old_warehouse
    assert type(new_warehouse) is IntType, "new_warehouse is not an integer: %r" % new_warehouse
    total = 0
    try_it = 0
    for old_one in WAREHOUSES[str(old_warehouse)]:
        new_cost = get_costs(new_warehouse,old_one[0])
        old_cost = float(old_one[0])
        lucro = old_cost - new_cost
        if lucro > 0:
            total += lucro
            #print
            #print "COMPARE-old_one\t",old_warehouse,old_one,"\t", \
            old_one[2],"\t", lucro
    if total == 0:
            pass
            #print 80 * "*"
            #print "nao tem lucro ", old_warehouse,"\t=>",new_warehouse
    else:
            #print 80 * "*"
            old_open_cost = get_open_cost(str(old_warehouse))
            new_open_cost = get_open_cost(str(new_warehouse))
            try_it = old_open_cost - new_open_cost +total
            if try_it > 0:
              if message:
                print
                print "TEM LUCRO ", old_warehouse," => ",new_warehouse,"\t", total,\
                      "\t", old_open_cost, new_open_cost, try_it
    #return try_it,old_warehouse,new_warehouse
    return [try_it,old_warehouse,new_warehouse]

def warehouse_is_open(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    return warehouse in open_warehouses

def get_costs(warehouse,customer):
    if not type(customer) is IntType:
        customer = int(customer)
    assert type(warehouse) is IntType, "warehouse is not an integer: %r" % warehouse
    assert type(customer)  is IntType, "customer is not an integer: %r" % customer
    #print warehouse,customer
    return costs[warehouse][customer]

def get_demand(customer):
    assert type(customer)  is StringType, "customer is not a String: %r" % customer
    return demand[customer]

def get_open_cost(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    return supplyData[warehouse][1]

def get_capacity(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    return supplyData[warehouse][0]

def get_supplyData():
    liste = []
    for plant in Plants:
        liste.append( [get_open_cost(plant),get_capacity(plant)] )
    return liste

def get_used_capacity(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    #print warehouse,WAREHOUSES[warehouse]
    return sum([ x[1] for x in  WAREHOUSES[warehouse] ])

def get_used_capacity_of_all():
    liste = []
    for plant in Plants:
        liste.append(get_used_capacity(plant))
    return liste

def list_warehouses_costs():
    for idx, warehouse in enumerate(costs):
        #for customer in warehouse:
        summe = sum([ x for x in warehouse])
        print idx, summe #warehouse

def get_warehouses_by_sum():
    wlist = []
    for idx, warehouse in enumerate(costs):
        #for customer in warehouse:
        summe = sum([ x for x in warehouse])
        #print idx, summe #warehouse
        xxx = get_open_cost(str(idx))
        #print "open_cost",open_cost
        #print "SUMME ", summe
        summe += xxx
        #print "SUMME+XXX ", summe
        wlist.append([summe,idx])

    #print wlist
    wlist = sorted(wlist, key = itemgetter(0))
    #print wlist
    #me_sorted = [x[0] for x in wlist]
    #print "WLIST soreted\n",wlist, "\n"
    #print "----------------------"
    return [ str(x[1]) for x in wlist]

def get_warehouses_by_sum_only_warehouses():
    wlist = []
    for idx, warehouse in enumerate(costs):
        #summe = sum([ x for x in warehouse])
        xxx = get_open_cost(str(idx))
        summe = xxx
        wlist.append([summe,idx])
    wlist = sorted(wlist, key = itemgetter(0))
    return [ str(x[1]) for x in wlist]

def get_warehouses_by_sum_only_open_customers():
    wlist = []
    for idx, warehouse in enumerate(costs):
        summe = sum([ x for x in warehouse])
        #xxx = get_open_cost(str(idx))
        #summe = xxx
        wlist.append([summe,idx])
    wlist = sorted(wlist, key = itemgetter(0))
    return [ str(x[1]) for x in wlist]

def list_of_customers(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    #for x in WAREHOUSES[warehouse]:
    return [ x[0] for x in WAREHOUSES[warehouse]]

def solve_me(inputData):
    global num_warehouses, num_customers, TOTAL_COST
    global Plants, supplyData
    global Stores, demand, costs
    global open_warehouses, closed_ones, customer_warehouse

    DEBUG = False

    lines = inputData.split('\n')
    num_warehouses = int(lines[0].split()[0])
    num_customers  = int(lines[0].split()[1])

    # Creates a list of all the supply nodes
    Plants = [ str(x) for x in range(num_warehouses)]

    supplyData = {  str(idx):[ int(k.split()[0]), float(k.split()[1])]
                        for idx,k in enumerate(lines[1:num_warehouses+1])
                 }

    Stores = [ str(x) for x in range(num_customers)]

    demand = { str(idx):int(k) for idx,k in enumerate( lines[num_warehouses+1::2] )
                 if len(k.split()) == 1
               }

    costs = []
    for x in range(num_customers):
        costs.append([])
    for idx,x in enumerate( lines[num_warehouses+2::2]):
        for y in x.split():
          costs[idx].append(float(y))

    costs = swapaxes(array(costs),0,1).tolist()

    #list_warehouses_costs()
    # wlist = get_warehouses_by_sum()
    #for w in wlist:
    #    print w

    if DEBUG:
        print num_warehouses, num_customers
        print "Plants ",Plants
        print "supplyData ", supplyData
        print "Stores ", Stores
        print "demand ", demand
        # print costs

    #print "Initialization done"
    #hd = warehouse_with_highest_demand()
    #print "warehouse_with_highest_demand()",hd

    #total_demand = sum(demand.itervalues())
    #print "total_demand",total_demand
    #total_capacity = sum( [ value[0] for key, value in supplyData.items()] )
    #print "total_capacity",total_capacity

    #warehouses_sorted_by_demand = list(sorted(supplyData, key=supplyData.__getitem__, reverse=True))
    #print warehouses_sorted_by_demand

    #warehouses_sorted_by_demand = get_warehouses_by_sum()
    warehouses_sorted_by_demand = get_warehouses_by_sum_only_warehouses()
    #print warehouses_sorted_by_demand

    #for warehouse in warehouses_sorted_by_demand:
    #    print warehouse,"\t",  get_open_cost(str(warehouse)),\
    #          "\t", get_capacity(warehouse),\
    #          "\t" #,sum([ x for x in warehouse])
    #only_open_warehouses =  get_warehouses_by_sum_only_warehouses()

    #only_customers =  get_warehouses_by_sum_only_customers()
    #for warehouse in only_open_warehouses:
    #    print warehouse,"\t",  get_open_cost(str(warehouse))

    #for warehouse in only_customers:
    #    print warehouse,"\t",  get_open_cost(str(warehouse))

    #warehouses_sorted_by_demand = list(sorted(supplyData, key=itemgetter(1), reverse=True))
    #get_cheapest_in_front = []
    #for x in warehouses_sorted_by_demand:
    #    #print supplyData[x]
    #    if supplyData[x][1] == 0.0:
    #      #print "here i am"
    #      get_cheapest_in_front.append(x)
    #      warehouses_sorted_by_demand.pop(int(x))
    #warehouses_sorted_by_demand = get_cheapest_in_front + warehouses_sorted_by_demand


    #print "warehouses_sorted_by_demand",warehouses_sorted_by_demand
    #print supplyData
    TOTAL_DEMAND = 0
    #TOTAL_COST = 0
    done = False

    blacklist = []
    for warehouse in warehouses_sorted_by_demand:
          if warehouse in WAREHOUSES:
              continue
          if done:
              break
          # open a warehouse
          WAREHOUSES[warehouse] = []

          working_total_capacity = supplyData[warehouse][0]
          working_capacity  = 0

          while working_capacity < working_total_capacity:

              if sum([int(x) for x in  Stores ]) < 0:
                  done = True
                  break;
              # = next Costumer, min cost for actual warehouse
              next_one =  get_next_costumer(warehouse,blacklist)
              if next_one == -1:
                  break
              customer_demand = demand[ str(next_one) ]
              if customer_demand + working_capacity <= working_total_capacity:

                  customer = next_one
                  blacklist.append(next_one)

                  cost = costs[int(warehouse)][int(customer)]
                  WAREHOUSES[warehouse].append( (customer, customer_demand, cost) )

                  TOTAL_COST += cost
                  TOTAL_DEMAND += customer_demand

                  working_capacity += customer_demand
              else:
                  break




    #customer_warehouse = {}
    for key, value in  WAREHOUSES.items():
        if value == [] : continue
        for c in value:
          customer_warehouse[ int(c[0]) ] = key


    wh = [ key for key, value in WAREHOUSES.items() if value != []]
    for w in wh:
        open_warehouses.append(w)
        TOTAL_COST = TOTAL_COST + supplyData[w][1]


    #print "list_of_customers",list_of_customers('56')
    avg = TOTAL_COST / num_customers
    print "avg ", avg

    closed_ones = [ str(x) for x in range(num_warehouses) if not str(x) in open_warehouses]
    print "opened ", open_warehouses


    print 80 * '*'
    print_data()

    #berechne_kosten(1230,0)
    #berechne_kosten(1230,2)
    #for i in range(num_customers):
    #    berechne_kosten(1230,i)

    #print "NC",num_customers

#==============================================================================
#
#     for i in range(100):
#         berechne_kosten(0,i)
#
#     for i in range(99):
#         berechne_kosten(1,i)
#
#     for i in range(99):
#         berechne_kosten(8,i)
#     for i in range(99):
#         berechne_kosten(51,i)
#     for i in range(99):
#         berechne_kosten(64,i)
#     for i in range(99):
#         berechne_kosten(90,i)
#     for i in range(99):
#         berechne_kosten(58,i)
#
#
#     WX = deepcopy(WAREHOUSES)
#     for idx,x in enumerate( WX['1']):
#
#       SWAP(1,58,int(x[0]))
#==============================================================================
    #WX = deepcopy(WAREHOUSES)
    #for idx,x in enumerate( WX['1']):
    #   SWAP(1,90,int(x[0]))

    #berechne_kosten(8,90)
    #berechne_kosten(8,58)
    #print "56-all=>",x_all_customer_costs('56')
    #print "56-Liste=>",x_customerlist_costs('56',list_of_customers('56'))
    #print "57-all=>",x_all_customer_costs('57')
    #print "57-Liste=>",x_customerlist_costs('57',list_of_customers('56'))

    #print "56_open=>",get_open_cost('56')
    #print "57=>",x_all_customer_costs('57')
    #
    #close_warehouse('58')

    #analyze_swap_warehouse('56','57')
    #x_swap_warehouse('56','57')
    repetitions = 500
    for x in range(repetitions):
        shuffle(open_warehouses)
        old_one = open_warehouses[0]
        shuffle(closed_ones)
        new_one = closed_ones[0]
        lucro = analyze_swap_warehouse(old_one,new_one)
        if lucro > 0:
            print "Iteration",x,"\tSWAPPING-LUCRO",lucro,"\t",old_one,"=>",new_one
            x_swap_warehouse(old_one,new_one)
        else:
            print "Iteration",x,"\tNOTHING TO DO",lucro,"\t",old_one,"xx",new_one

#==============================================================================
#     ...poucos errors
#     repetitions = 50
#     for x in range(repetitions):
#         shuffle(open_warehouses)
#         old_one = open_warehouses[0]
#         print "OO",old_one
#         x_swap_customer(old_one)
#==============================================================================

    for wh in open_warehouses:
         print "\nWarehouse-Capacity-UsedCapacity:", wh, get_capacity(str(wh)),get_used_capacity(str(wh)),get_open_cost(str(wh)),
         #print "\t",len(WAREHOUSES[str(wh)]),"\t", sum(WAREHOUSES[str(wh)][1]),"\t",sum(WAREHOUSES[str(wh)][2])

    print "\nOpen Warehouses: ", open_warehouses
    print "Customer", num_customers
    print "Warehouses", num_warehouses

    output = str(TOTAL_COST) + " 0\n"
    output += " ".join([ customer_warehouse[x] for x in customer_warehouse])

    return output
#==============================================================================
#
#==============================================================================
def x_swap_warehouse(old,new):
    """
    old, new : String
    """
    #print "i am here"
    global TOTAL_COST, customer_warehouse
    clist = list_of_customers(old)
    #WAREHOUSES[warehouse].append( (customer, customer_demand, cost) )
    #print "AAA", TOTAL_COST
    for x in clist:
        TOTAL_COST -= get_costs(int(old),x)
        new_cost = get_costs(int(new),x)
        TOTAL_COST += new_cost
        WAREHOUSES[new].append( (x, get_demand(str(x)), new_cost) )
        customer_warehouse[int(x)] = new
        #TOTAL_DEMAND -= get_demand(x)
    #print "BBB", TOTAL_COST
    WAREHOUSES[old] = []

    close_warehouse(old)
    open_warehouse(new)
    #print customer_warehouse
#==============================================================================
def x_swap_customer(old):
    global TOTAL_COST, customer_warehouse
    for wh in open_warehouses:
      if wh == int(old) : continue

      for x in WAREHOUSES[old]:
          print x
          customer = x[0]
          old_cost = get_costs(int(old),customer)
          new_cost = get_costs(int(wh),customer)
          #customer = int(WAREHOUSES[x][2])
          lucro = old_cost - new_cost
          if lucro > 0:
                print "Iteration",x,"\tCUSTOMER-SWAPPING-LUCRO",customer,"\t",lucro,"\t",old,"=>",wh
                TOTAL_COST -= old_cost
                TOTAL_COST += new_cost
                WAREHOUSES[old] = [ x for x in WAREHOUSES[old] if x[0] != customer]
                WAREHOUSES[str(wh)].append( (x, get_demand(str(x[0])), new_cost) )
                customer_warehouse[int(x[0])] = wh
          else:
                print "Iteration",x,"\tCUSTOMER-NOTHING TO DO",customer,"\t",lucro,"\t",old,"xx",wh

#
def analyze_swap_warehouse(old,new):
    """
    old, new : String
    """
    #retval = []
    clist = list_of_customers(old)
    open_cost_old =  get_open_cost(old)
    open_cost_new =  get_open_cost(new)

    old_cost = sum([ get_costs(int(old),x) for x in clist])
    new_cost = sum([ get_costs(int(new),x) for x in clist])
    #retval.append(open_cost_old)
    #retval.append(old_cost)
    #retval.append(open_cost_new)
    #retval.append(new_cost)
    #retval.append(old_cost)
    #retval.append(old_cost+open_cost_old)
    #retval.append(new_cost+open_cost_new)
    #retval.append( (old_cost+open_cost_old) - (new_cost+open_cost_new))
    return (old_cost+open_cost_old) - (new_cost+open_cost_new)


#==============================================================================
def x_all_customer_costs(warehouse):
    retval = sum([ x[2] for x in WAREHOUSES[warehouse]])
    return retval
#==============================================================================
def x_customerlist_costs(warehouse,clist):
    print "\nCLIST",clist
    retval = sum([ get_costs(int(warehouse),x) for x in clist])
    print  "\nCLIST-WAREHOUSE-SUMME", warehouse, retval
    return retval
#==============================================================================
def local_swap():
    """
    swap warehouse for 1-item-entries
    useful for problem 1
    """
    global TOTAL_COST, open_warehouses, closed_ones
    #print "openones before",open_warehouses
    #print "closed before",closed_ones
    #print "TOTAL before",TOTAL_COST
    #print " ".join([ customer_warehouse[x] for x in customer_warehouse])
    for warehouse in open_warehouses:
        if len(WAREHOUSES[warehouse]) == 1:

            customer =  WAREHOUSES[warehouse][0][0]
            demand   =  WAREHOUSES[warehouse][0][1]
            old_cost  =  WAREHOUSES[warehouse][0][2]
            closed_ones_copy = deepcopy(closed_ones)
            for closed in closed_ones_copy:

                new_cost = get_costs(int(closed),customer)

                old_warehouse = warehouse
                new_warehouse = closed
                if new_cost < old_cost:
                    WAREHOUSES[old_warehouse].pop(0)
                    WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
                    TOTAL_COST -= old_cost
                    TOTAL_COST += new_cost
                    TOTAL_COST += get_open_cost(new_warehouse)
                    TOTAL_COST -= get_open_cost(old_warehouse)

                    open_warehouses.append(new_warehouse)
                    open_warehouses = [x for x in open_warehouses if x != old_warehouse]
                    closed_ones.append(old_warehouse)
                    closed_ones = [x for x in closed_ones if x != new_warehouse]

                    customer_warehouse[customer] = new_warehouse
                    print "Local-Swap ",old_warehouse," TO ",new_warehouse
                    break

def SWAP(old_warehouse,new_warehouse,customer):
    global TOTAL_COST, open_warehouses, closed_ones, customer_warehouse

    old_cost =  get_costs(old_warehouse,customer)
    new_cost =  get_costs(new_warehouse,customer)
    old_index = get_customer_index(old_warehouse,customer)
    #print "OLD_INDEX", old_index
    if old_index != -1:
      WAREHOUSES[str(old_warehouse)].pop(old_index)
      WAREHOUSES[str(new_warehouse)].append( (customer,get_demand(str(customer)),new_cost) )
      TOTAL_COST -= old_cost
      TOTAL_COST += new_cost
      #print "SWAP ", old_warehouse,new_warehouse,customer, old_cost - new_cost
      customer_warehouse[customer] = str(new_warehouse)

def MOVE_TO_AND_OPEN_IF_CLOSED(old_warehouse,new_warehouse):
    global TOTAL_COST, open_warehouses, closed_ones, customer_warehouse

    if not warehouse_is_open(new_warehouse):
        print
        print "WAREHOUSE OPENED"
        open_warehouse(new_warehouse)

    WX = deepcopy(WAREHOUSES)
    for line in WX[str(old_warehouse)]:
       customer = str(line[0])
       old_cost =  get_costs(int(old_warehouse),int(customer))
       new_cost =  get_costs(int(new_warehouse),int(customer))
       old_index = get_customer_index(old_warehouse,customer)
       #print "Line", line
       #print "CUSTOMER", customer
       #print "OLD_WAREHOUSE", old_warehouse
       #print "OLD_INDEX", old_index
       if new_cost >= old_cost:
           continue

       if old_index != -1:
        WAREHOUSES[str(old_warehouse)].pop(old_index)
        WAREHOUSES[str(new_warehouse)].append( (customer,get_demand(str(customer)),new_cost) )
        TOTAL_COST -= old_cost
        TOTAL_COST += new_cost
        customer_warehouse[int(customer)] = str(new_warehouse)

def get_customer_index(warehouse,customer):
    retval = -1
    w = WAREHOUSES[str(warehouse)]
    for idx, wx in enumerate(w):
        #print wx, wx[0], customer, wx[0] == customer, type(wx[0]), type(customer)
        if wx[0] == int(customer):
            #print "found it at ", idx
            retval = idx
            break
    return retval

def berechne_kosten(old_warehouse,new_warehouse):
    global TOTAL_COST, open_warehouses, closed_ones, customer_warehouse
    lucro_total = 0
    #new_warehouse = 0
    is_open = False
    for customer in range(num_customers):
         old_cost =  get_costs(old_warehouse,customer)
         new_cost =  get_costs(new_warehouse,customer)
         lucro = old_cost - new_cost
         #print "LUCRO ", lucro, old_cost, new_cost
         old_index = get_customer_index(old_warehouse,customer)
         if lucro > 0 and old_index >= 0:
             lucro_total += lucro
         #    SWAP(old_warehouse, new_warehouse,customer)
         #    if not is_open:
         #        is_open = True
         #        TOTAL_COST += get_open_cost(str(new_warehouse))
         #        open_warehouses.append(new_warehouse)
         #        open_warehouses = [x for x in open_warehouses if x != old_warehouse]
         #        closed_ones.append(old_warehouse)
         #        closed_ones = [x for x in closed_ones if x != new_warehouse]

    oc = get_open_cost(str(new_warehouse))
    #print "LUCRO-Total1 ",new_warehouse,"\t",lucro_total,"\t",oc
    #print "LUCRO-Total2 ",new_warehouse,"\t",lucro_total-oc,"\t"

    is_open = False
    if lucro_total-oc > 0:
      print "LUCRO-Total1 ",new_warehouse,"\t",lucro_total,"\t",oc
      print "LUCRO-Total2 ",new_warehouse,"\t",lucro_total-oc,"\t"
      for customer in range(num_customers):
         old_cost =  get_costs(old_warehouse,customer)
         new_cost =  get_costs(new_warehouse,customer)
         lucro = old_cost - new_cost
         #print "LUCRO ", lucro, old_cost, new_cost
         old_index = get_customer_index(old_warehouse,customer)
         if lucro > 0 and old_index >= 0:
             lucro_total += lucro
             SWAP(old_warehouse, new_warehouse,customer)

             if not is_open:
                 is_open = True
                 if  not str(new_warehouse) in open_warehouses:
                  TOTAL_COST += get_open_cost(str(new_warehouse))
                  open_warehouses.append(str(new_warehouse))
                  open_warehouses = [x for x in open_warehouses if x != old_warehouse]
                  closed_ones.append(old_warehouse)
                  closed_ones = [x for x in closed_ones if x != new_warehouse]

def print_data():
    mm = get_used_capacity_of_all()
    for idx,x in enumerate(mm):
        #if len(WAREHOUSES[str(idx)]) != 1: continue
        if x > 0:
           print idx,"\t",x,"\t",get_open_cost(str(idx)) # ,"\t",WAREHOUSES[str(idx)]
        #customer = WAREHOUSES[str(idx)][0][0]

    #print "Closed Warehouses: ",closed_ones

def get_warehouse4insert(old_warehouse,customer,avg):

    warehouse_to_insert = -1
    demand = get_demand(str(customer))
    old_cost = get_costs(int(old_warehouse),customer)
    lucro = 0

    for warehouse in open_warehouses:
        if warehouse == old_warehouse: continue
        used_capacity = get_used_capacity(warehouse)
        capacity = get_capacity(warehouse)
        #print used_capacity, capacity
        #free_capacity = capacity - used_capacity
        if used_capacity + demand < capacity:
             #return warehouse
             new_cost = get_costs(int(warehouse),customer)
             if new_cost < old_cost:
                 if old_cost - new_cost > lucro:
             #if 1==1:
                     warehouse_to_insert = warehouse
                     lucro = old_cost - new_cost
        #print WAREHOUSES[warehouse] #WAREHOUSES[str(customer)]
        #print sum([ x[1] for x in  WAREHOUSES[warehouse] ])
    return warehouse_to_insert

def get_warehouse2open(old_warehouse,customer,avg):

    warehouse_to_open = -1
    #demand = get_demand(str(customer))
    old_cost = get_costs(int(old_warehouse),customer)
    lucro = 0
    #print "OLD_WAREHOUSE ",old_warehouse
    for warehouse in closed_ones:
        if warehouse == old_warehouse: continue
        #used_capacity = get_used_capacity(warehouse)
        #capacity = get_capacity(warehouse)

        #if used_capacity + demand < capacity:
        new_cost = get_costs(int(warehouse),customer)
        if new_cost < old_cost:
             if old_cost - new_cost > lucro:
                     warehouse_to_open = warehouse
                     lucro = old_cost - new_cost

    return warehouse_to_open

def swap_warehouse(old_warehouse,customer):
    pass

def close_warehouse(warehouse):
    global TOTAL_COST, open_warehouses, closed_ones
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    if warehouse in closed_ones:
        print "SCHON GESCHLOSSEN ", warehouse
        return
    TOTAL_COST -= get_open_cost(warehouse)
    print "CLOSED", warehouse
    closed_ones.append(warehouse)
    open_warehouses = [x for x in open_warehouses if x != warehouse]

def open_warehouse(new_warehouse):
    global TOTAL_COST, open_warehouses, closed_ones
    if new_warehouse in open_warehouses:
        return
    TOTAL_COST += get_open_cost(new_warehouse)
    print "OPENED", new_warehouse
    open_warehouses.append(new_warehouse)
    closed_ones = [x for x in closed_ones if x != new_warehouse]

def swap_customer(old_warehouse,customer, avg):
    global TOTAL_COST, open_warehouses, closed_ones, customer_warehouse

    #lucro = 0

    old_cost = get_costs(int(old_warehouse),customer)
    if old_cost < avg: return
    #new_cost = get_costs(int(new_warehouse),customer)
    demand = get_demand(str(customer))

    #print old_cost, demand
    #print get_capacity(old_warehouse)

    new_warehouse = get_warehouse4insert(old_warehouse,customer,avg)

    if new_warehouse == -1:
        #return
        #print "no insert possible, have to open new_one"
        new_warehouse =get_warehouse2open(old_warehouse,customer,avg)
        #print "NEW_WAREHOUSE", new_warehouse
        if new_warehouse == -1:
            pass # nothing to do
        else:
            new_cost = get_costs(int(new_warehouse),customer)
            index = -1
            for idx,x in enumerate(WAREHOUSES[old_warehouse]):
                if x[0] == customer:
                     index = idx
            WAREHOUSES[old_warehouse].pop(index)
            WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
            TOTAL_COST -= old_cost
            TOTAL_COST += new_cost
            TOTAL_COST += get_open_cost(new_warehouse)
            open_warehouses.append(new_warehouse)
            closed_ones = [x for x in closed_ones if x != new_warehouse]
            customer_warehouse[customer] = new_warehouse
    else:
        #print "inserting"
        new_cost = get_costs(int(new_warehouse),customer)
        lucro = old_cost - new_cost
        index = -1
        if lucro > 0:
            #index = -1
            for idx,x in enumerate(WAREHOUSES[old_warehouse]):
                if x[0] == customer:
                     index = idx
        WAREHOUSES[old_warehouse].pop(index)
        WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
        TOTAL_COST -= old_cost
        TOTAL_COST += new_cost
        #TOTAL_COST += get_open_cost(new_warehouse)
        #open_warehouses.append(new_warehouse)
        #closed_ones = [x for x in closed_ones if x != new_warehouse]
        customer_warehouse[customer] = new_warehouse
        #print  " ".join([ customer_warehouse[x] for x in customer_warehouse])



    #open_cost = get_open_cost(new_warehouse)
    #if old_cost < new_cost + open_cost:
    #    return
    #if old_cost < avg:
    #    return
    """
    index = -1
    for idx,x in enumerate(WAREHOUSES[old_warehouse]):
        if x[0] == customer:
            index = idx

    WAREHOUSES[old_warehouse].pop(index)

    WAREHOUSES[new_warehouse].append( (customer,d,old_cost) )
    TOTAL_COST -= old_cost
    TOTAL_COST += new_cost
    TOTAL_COST += get_open_cost(new_warehouse)
    open_warehouses.append(new_warehouse)
    closed_ones = [x for x in closed_ones if x != new_warehouse]

    customer_warehouse[customer] = new_warehouse
    """

import sys
if __name__ == '__main__':
    if len(sys.argv) > 1:
        fileLocation = sys.argv[1].strip()
        inputDataFile = open(fileLocation, 'r')
        inputData = ''.join(inputDataFile.readlines())
        inputDataFile.close()
        time_now = time()
        print solve_me(inputData)
        time_end = time();
        elapsed = time_end - time_now
        print 'running '+ repr(round(elapsed,5))  + ' seconds.'
    else:
        print 'This test requires an input file. Please select one from the data directory. (i.e. python solver.py ./data/wl_16_1)'