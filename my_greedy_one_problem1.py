# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 06:55:49 2013

@author: uwe
"""
from time import time
from numpy import array, swapaxes
from operator import itemgetter
from types import *
from copy import deepcopy

num_warehouses = 0
num_customers = 0
Plants = None
supplyData = None
Stores =None
demand = None
costs = None

WAREHOUSES = { }
TOTAL_COST = 0
open_warehouses = []
closed_ones = []
customer_warehouse = { }
#avg = 0

def warehouse_with_highest_demand():
    return max(supplyData, key=supplyData.get)

def get_next_costumer(warehouse,blacklist):

    minval = 1000000000.0
    index  = -1
    for idx, cost in enumerate(costs[int(warehouse)]):
        if idx in blacklist: continue
        if cost < minval:
            minval = cost
            index  = idx
    return index

def get_costs(warehouse,customer):
    assert type(warehouse) is IntType, "warehouse is not an integer: %r" % warehouse
    assert type(customer)  is IntType, "customer is not an integer: %r" % customer
    return costs[warehouse][customer]

def get_demand(customer):
    assert type(customer)  is StringType, "customer is not a String: %r" % customer
    return demand[customer]

def get_open_cost(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    return supplyData[warehouse][1]

def get_capacity(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    return supplyData[warehouse][0]

def get_supplyData():
    liste = []
    for plant in Plants:
        liste.append( [get_open_cost(plant),get_capacity(plant)] )
    return liste

def get_used_capacity(warehouse):
    assert type(warehouse)  is StringType, "warehouse is not a String: %r" % warehouse
    #print warehouse,WAREHOUSES[warehouse]
    return sum([ x[1] for x in  WAREHOUSES[warehouse] ])

def get_used_capacity_of_all():
    liste = []
    for plant in Plants:
        liste.append(get_used_capacity(plant))
    return liste

def list_warehouses_costs():
    for idx, warehouse in enumerate(costs):
        #for customer in warehouse:
        summe = sum([ x for x in warehouse])
        print idx, summe #warehouse

def get_warehouses_by_sum():
    wlist = []
    for idx, warehouse in enumerate(costs):
        #for customer in warehouse:
        summe = sum([ x for x in warehouse])
        #print idx, summe #warehouse
        xxx = get_open_cost(str(idx))
        #print "open_cost",open_cost
        #print "SUMME ", summe
        summe += xxx
        #print "SUMME+XXX ", summe
        wlist.append([summe,idx])

    #print wlist
    wlist = sorted(wlist, key = itemgetter(0))
    #print wlist
    #me_sorted = [x[0] for x in wlist]
    #print "WLIST soreted\n",wlist, "\n"
    #print "----------------------"
    return [ str(x[1]) for x in wlist]

def solve_me(inputData):
    global num_warehouses, num_customers, TOTAL_COST
    global Plants, supplyData
    global Stores, demand, costs
    global open_warehouses, closed_ones, customer_warehouse

    DEBUG = False

    lines = inputData.split('\n')
    num_warehouses = int(lines[0].split()[0])
    num_customers  = int(lines[0].split()[1])

    print "hi there"
    print num_warehouses
    print num_customers
    return
    # Creates a list of all the supply nodes
    Plants = [ str(x) for x in range(num_warehouses)]

    supplyData = {  str(idx):[ int(k.split()[0]), float(k.split()[1])]
                        for idx,k in enumerate(lines[1:num_warehouses+1])
                 }

    Stores = [ str(x) for x in range(num_customers)]

    demand = { str(idx):int(k) for idx,k in enumerate( lines[num_warehouses+1::2] )
                 if len(k.split()) == 1
               }

    costs = []
    for x in range(num_customers):
        costs.append([])
    for idx,x in enumerate( lines[num_warehouses+2::2]):
        for y in x.split():
          costs[idx].append(float(y))

    costs = swapaxes(array(costs),0,1).tolist()

    #list_warehouses_costs()
    # wlist = get_warehouses_by_sum()
    #for w in wlist:
    #    print w

    if DEBUG:
        print num_warehouses, num_customers
        print "Plants ",Plants
        print "supplyData ", supplyData
        print "Stores ", Stores
        print "demand ", demand
        # print costs

    #print "Initialization done"
    #hd = warehouse_with_highest_demand()
    #print "warehouse_with_highest_demand()",hd

    #total_demand = sum(demand.itervalues())
    #print "total_demand",total_demand
    #total_capacity = sum( [ value[0] for key, value in supplyData.items()] )
    #print "total_capacity",total_capacity

    #warehouses_sorted_by_demand = list(sorted(supplyData, key=supplyData.__getitem__, reverse=True))
    #print warehouses_sorted_by_demand

    warehouses_sorted_by_demand = get_warehouses_by_sum()
    #print warehouses_sorted_by_demand

    #warehouses_sorted_by_demand = list(sorted(supplyData, key=itemgetter(1), reverse=True))
    #get_cheapest_in_front = []
    #for x in warehouses_sorted_by_demand:
    #    #print supplyData[x]
    #    if supplyData[x][1] == 0.0:
    #      #print "here i am"
    #      get_cheapest_in_front.append(x)
    #      warehouses_sorted_by_demand.pop(int(x))
    #warehouses_sorted_by_demand = get_cheapest_in_front + warehouses_sorted_by_demand


    #print "warehouses_sorted_by_demand",warehouses_sorted_by_demand
    #print supplyData
    TOTAL_DEMAND = 0
    #TOTAL_COST = 0
    done = False

    blacklist = []
    for warehouse in warehouses_sorted_by_demand:
          if warehouse in WAREHOUSES:
              continue
          if done:
              break
          # open a warehouse
          WAREHOUSES[warehouse] = []

          working_total_capacity = supplyData[warehouse][0]
          working_capacity  = 0

          while working_capacity < working_total_capacity:

              if sum([int(x) for x in  Stores ]) < 0:
                  done = True
                  break;
              # = next Costumer, min cost for actual warehouse
              next_one =  get_next_costumer(warehouse,blacklist)
              if next_one == -1:
                  break
              customer_demand = demand[ str(next_one) ]
              if customer_demand + working_capacity <= working_total_capacity:

                  customer = next_one
                  blacklist.append(next_one)

                  cost = costs[int(warehouse)][int(customer)]
                  WAREHOUSES[warehouse].append( (customer, customer_demand, cost) )
                  TOTAL_COST += cost
                  TOTAL_DEMAND += customer_demand

                  working_capacity += customer_demand
              else:
                  break




    #customer_warehouse = {}
    for key, value in  WAREHOUSES.items():
        if value == [] : continue
        for c in value:
          customer_warehouse[ int(c[0]) ] = key


    wh = [ key for key, value in WAREHOUSES.items() if value != []]
    for w in wh:
        open_warehouses.append(w)
        TOTAL_COST = TOTAL_COST + supplyData[w][1]


    avg = TOTAL_COST / num_customers
    print "avg ", avg

    closed_ones = [ str(x) for x in range(num_warehouses) if not str(x) in open_warehouses]
    print "opened ", open_warehouses
    print "closed ", closed_ones

    for ii in range(1):
        correct_1 =  [ [key,value] for key, value in WAREHOUSES.items() if value != []]
        try_to_swap = []
        for warehouse in correct_1:
          if 1 == 1:
              try_to_swap.append(warehouse)
          try_to_swap = try_to_swap[::-1]
          for tts in try_to_swap:
             liste = tts[1]
             for elem in liste:
                 old_warehouse = tts[0]
                 customer = elem[0]
                 swap_customer(old_warehouse,customer,avg)


    print_data()

    for i in range(2):
       local_swap()
    print 80 * '*'
    print_data()

    output = str(TOTAL_COST) + " 0\n"
    output += " ".join([ customer_warehouse[x] for x in customer_warehouse])

    return output

def local_swap():
    """
    swap warehouse for 1-item-entries
    """
    global TOTAL_COST, open_warehouses, closed_ones
    #print "openones before",open_warehouses
    #print "closed before",closed_ones
    #print "TOTAL before",TOTAL_COST
    #print " ".join([ customer_warehouse[x] for x in customer_warehouse])
    for warehouse in open_warehouses:
        if len(WAREHOUSES[warehouse]) == 1:

            customer =  WAREHOUSES[warehouse][0][0]
            demand   =  WAREHOUSES[warehouse][0][1]
            old_cost  =  WAREHOUSES[warehouse][0][2]
            closed_ones_copy = deepcopy(closed_ones)
            for closed in closed_ones_copy:

                new_cost = get_costs(int(closed),customer)

                old_warehouse = warehouse
                new_warehouse = closed
                if new_cost < old_cost:
                    WAREHOUSES[old_warehouse].pop(0)
                    WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
                    TOTAL_COST -= old_cost
                    TOTAL_COST += new_cost
                    TOTAL_COST += get_open_cost(new_warehouse)
                    TOTAL_COST -= get_open_cost(old_warehouse)

                    open_warehouses.append(new_warehouse)
                    open_warehouses = [x for x in open_warehouses if x != old_warehouse]
                    closed_ones.append(old_warehouse)
                    closed_ones = [x for x in closed_ones if x != new_warehouse]

                    customer_warehouse[customer] = new_warehouse
                    print "Local-Swap ",old_warehouse," TO ",new_warehouse
                    #print "openones after",open_warehouses
                    #print "closedafter",closed_ones
                    #print "TOTALafter",TOTAL_COST
                    #print " ".join([ customer_warehouse[x] for x in customer_warehouse])
                    break
            #print customer

def print_data():
    mm = get_used_capacity_of_all()
    for idx,x in enumerate(mm):
        #if len(WAREHOUSES[str(idx)]) != 1: continue
        if x > 0:
           print idx,"\t",x,"\t",get_open_cost(str(idx)),"\t",WAREHOUSES[str(idx)]
        #customer = WAREHOUSES[str(idx)][0][0]

    #print "Closed Warehouses: ",closed_ones

def get_warehouse4insert(old_warehouse,customer,avg):

    warehouse_to_insert = -1
    demand = get_demand(str(customer))
    old_cost = get_costs(int(old_warehouse),customer)
    lucro = 0

    for warehouse in open_warehouses:
        if warehouse == old_warehouse: continue
        used_capacity = get_used_capacity(warehouse)
        capacity = get_capacity(warehouse)
        #print used_capacity, capacity
        #free_capacity = capacity - used_capacity
        if used_capacity + demand < capacity:
             #return warehouse
             new_cost = get_costs(int(warehouse),customer)
             if new_cost < old_cost:
                 if old_cost - new_cost > lucro:
             #if 1==1:
                     warehouse_to_insert = warehouse
                     lucro = old_cost - new_cost
        #print WAREHOUSES[warehouse] #WAREHOUSES[str(customer)]
        #print sum([ x[1] for x in  WAREHOUSES[warehouse] ])
    return warehouse_to_insert

def get_warehouse2open(old_warehouse,customer,avg):

    warehouse_to_open = -1
    #demand = get_demand(str(customer))
    old_cost = get_costs(int(old_warehouse),customer)
    lucro = 0
    #print "OLD_WAREHOUSE ",old_warehouse
    for warehouse in closed_ones:
        if warehouse == old_warehouse: continue
        #used_capacity = get_used_capacity(warehouse)
        #capacity = get_capacity(warehouse)

        #if used_capacity + demand < capacity:
        new_cost = get_costs(int(warehouse),customer)
        if new_cost < old_cost:
             if old_cost - new_cost > lucro:
                     warehouse_to_open = warehouse
                     lucro = old_cost - new_cost

    return warehouse_to_open

def swap_warehouse(old_warehouse,customer):
    pass

def swap_customer(old_warehouse,customer, avg):
    global TOTAL_COST, open_warehouses, closed_ones, customer_warehouse

    #lucro = 0

    old_cost = get_costs(int(old_warehouse),customer)
    if old_cost < avg: return
    #new_cost = get_costs(int(new_warehouse),customer)
    demand = get_demand(str(customer))

    #print old_cost, demand
    #print get_capacity(old_warehouse)

    new_warehouse = get_warehouse4insert(old_warehouse,customer,avg)

    if new_warehouse == -1:
        #return
        #print "no insert possible, have to open new_one"
        new_warehouse =get_warehouse2open(old_warehouse,customer,avg)
        #print "NEW_WAREHOUSE", new_warehouse
        if new_warehouse == -1:
            pass # nothing to do
        else:
            new_cost = get_costs(int(new_warehouse),customer)
            index = -1
            for idx,x in enumerate(WAREHOUSES[old_warehouse]):
                if x[0] == customer:
                     index = idx
            WAREHOUSES[old_warehouse].pop(index)
            WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
            TOTAL_COST -= old_cost
            TOTAL_COST += new_cost
            TOTAL_COST += get_open_cost(new_warehouse)
            open_warehouses.append(new_warehouse)
            closed_ones = [x for x in closed_ones if x != new_warehouse]
            customer_warehouse[customer] = new_warehouse
    else:
        #print "inserting"
        new_cost = get_costs(int(new_warehouse),customer)
        lucro = old_cost - new_cost
        index = -1
        if lucro > 0:
            #index = -1
            for idx,x in enumerate(WAREHOUSES[old_warehouse]):
                if x[0] == customer:
                     index = idx
        WAREHOUSES[old_warehouse].pop(index)
        WAREHOUSES[new_warehouse].append( (customer,demand,new_cost) )
        TOTAL_COST -= old_cost
        TOTAL_COST += new_cost
        #TOTAL_COST += get_open_cost(new_warehouse)
        #open_warehouses.append(new_warehouse)
        #closed_ones = [x for x in closed_ones if x != new_warehouse]
        customer_warehouse[customer] = new_warehouse
        #print  " ".join([ customer_warehouse[x] for x in customer_warehouse])



    #open_cost = get_open_cost(new_warehouse)
    #if old_cost < new_cost + open_cost:
    #    return
    #if old_cost < avg:
    #    return
    """
    index = -1
    for idx,x in enumerate(WAREHOUSES[old_warehouse]):
        if x[0] == customer:
            index = idx

    WAREHOUSES[old_warehouse].pop(index)

    WAREHOUSES[new_warehouse].append( (customer,d,old_cost) )
    TOTAL_COST -= old_cost
    TOTAL_COST += new_cost
    TOTAL_COST += get_open_cost(new_warehouse)
    open_warehouses.append(new_warehouse)
    closed_ones = [x for x in closed_ones if x != new_warehouse]

    customer_warehouse[customer] = new_warehouse
    """



#==============================================================================
# def swap_customer(to_swap, avg):
#     global TOTAL_COST,open_warehouses, closed_ones
#     print "SWAP ",to_swap
#     old_warehouse = to_swap[0]
#     new_warehouse = to_swap[1]
#     customer      = to_swap[2]
#
#     old_cost = get_costs(int(old_warehouse),customer)
#     #print x
#     new_cost = get_costs(int(new_warehouse),customer)
#     d = get_demand(str(customer))
#
#     open_cost = get_open_cost(new_warehouse)
#     if old_cost < new_cost + open_cost:
#         return
#     if old_cost < avg:
#         return
#     index = -1
#     for idx,x in enumerate(WAREHOUSES[old_warehouse]):
#         if x[0] == customer:
#             index = idx
#
#     WAREHOUSES[old_warehouse].pop(index)
#
#     WAREHOUSES[new_warehouse].append( (customer,d,old_cost) )
#     TOTAL_COST -= old_cost
#     TOTAL_COST += new_cost
#     TOTAL_COST += get_open_cost(new_warehouse)
#     open_warehouses.append(new_warehouse)
#     closed_ones = [x for x in closed_ones if x != new_warehouse]
#==============================================================================

    #customer_warehouse[customer] = new_warehouse


import sys
if __name__ == '__main__':
    if len(sys.argv) > 1:
        fileLocation = sys.argv[1].strip()
        inputDataFile = open(fileLocation, 'r')
        inputData = ''.join(inputDataFile.readlines())
        inputDataFile.close()
        time_now = time()
        print solve_me(inputData)
        time_end = time();
        elapsed = time_end - time_now
        print 'running '+ repr(round(elapsed,5))  + ' seconds.'
    else:
        print 'This test requires an input file. Please select one from the data directory. (i.e. python solver.py ./data/wl_16_1)'
