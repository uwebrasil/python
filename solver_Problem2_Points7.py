#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import namedtuple
import math
from operator import itemgetter

Point = namedtuple("Point", ['x', 'y'])
Facility = namedtuple("Facility", ['index', 'setup_cost', 'capacity', 'location', 'used_capacity'])
Customer = namedtuple("Customer", ['index', 'demand', 'location'])
usedCapacities = {}

TOTAL = 0
solution = [-1]*1
facilities = []
customers = []
 
def length(point1, point2):
    return math.sqrt((point1.x - point2.x)**2 + (point1.y - point2.y)**2)

def minCost(customer,facilities,blacklist):
    global solution
    global customers
    m = 1000000
    
    i = -1
    for f in facilities:
        if f.index in facilities: continue
        l =  length(customer.location, f.location)   
        #if not f.index in  usedCapacities:
        #   usedCapacities[f.index] = 0.   
        #summe(fac,solution,customers),
        #if (l < m)  and (usedCapacities[f.index] + customer.demand <= f.capacity) :   
        if (l < m)  and (summe(f,solution,customers) + customer.demand <= f.capacity) :             
            m = l
            i = f.index    
        
    #if i in usedCapacities:
    #   usedCapacities[i] += customer.demand
    
    return i , m   

def get_used_capacity(c,f,sol):
    scap = sum([c[cp].demand for cp in sol if cp == f.index])
    print "f.index :",f.index, "f.capacity :", f.capacity
    return scap
        
def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    global TOTAL
    global solution
    global facilities
    # parse the input
    lines = input_data.split('\n')

    parts = lines[0].split()
    facility_count = int(parts[0])
    customer_count = int(parts[1])
    
    
    for i in range(1, facility_count+1):
        parts = lines[i].split()
        facilities.append(Facility(i-1, float(parts[0]), int(parts[1]), Point(float(parts[2]), float(parts[3])),0.0 ))
                
    #customers = []
    for i in range(facility_count+1, facility_count+1+customer_count):
        parts = lines[i].split()
        customers.append(Customer(i-1-facility_count, int(parts[0]), Point(float(parts[1]), float(parts[2]))))
  
    #facilities = sorted(facilities, key = itemgetter(1))
    facilities = sorted(facilities, key = itemgetter(2))
        
    # build a trivial solution
    # pack the facilities one by one until all the customers are served
    solution = [-1]*len(customers)
    capacity_remaining = [f.capacity for f in facilities]

    facility_index = 0
        
    for customer in customers:
            solution[customer.index], X = minCost(customer,facilities,[])
            TOTAL += X 
    
    ###       
    close_facility(1,customers,False)  
    show_result(facilities,solution,customers)  
    print
    close_facility(2,customers,False)  
    show_result(facilities,solution,customers) 
    print    
    for i in range(1):
      close_facility(1,customers,False)   
      show_result(facilities,solution,customers) 
      print
      close_facility(2,customers,False)    
      show_result(facilities,solution,customers)   
      print
      close_facility(3,customers,False)    
      show_result(facilities,solution,customers)  
      print
    ###
    #for ii in solution:
    #  if anzahl(ii,solution) == 0: continue
      
        
          
    ###
    used = [0]*len(facilities)
    for facility_index in solution:
        used[facility_index] = 1
        
    # calculate the cost of the solution
    obj = sum([f.setup_cost*used[f.index] for f in facilities])
    obj += TOTAL   
    
      
    # prepare the solution in the specified output format
    output_data = str(obj) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))

    return output_data

def summe(fac,solution,customers):      
       s = 0
       for idx,x in enumerate(solution):
           if x == fac.index:
               s += customers[idx].demand
       return s
       
def anzahl(index,solution):
    return [x for x in solution if x==index].count(index)              

def show_result(facilities,solution,customers):
    for fac in facilities:
       print "Facility: ",fac.index,"\tCapacity: ",fac.capacity, "\tsumme: ",summe(fac,solution,customers),"\tcount: ", [x for x in solution if x==fac.index].count(fac.index) 
       
def close_facility(index,customers,force):
    global TOTAL
    global solution
    global facilities
    
    single = [ x for x in solution if solution.count(x)  == index]   
    more = set( x for x in solution if solution.count(x) != index and solution.count(x) > 0)   
    
    for x in single:
        f = [a for a in facilities if a.index==x]
        if f[0].setup_cost == 0: continue
      
        xx =-1
        for idx,b in enumerate(solution):
            if b == x:
                xx = idx
        c = customers[xx]        
    
        cost = length(c.location, f[0].location) 
    
        lucro = 0
        swap = [-1,-1]
        for y in more:
            fx = [a for a in facilities if a.index==y]
            new_cost = length(c.location, fx[0].location) 
         
            if (cost+fx[0].setup_cost) - new_cost> 0:
              
                if (cost+fx[0].setup_cost) - new_cost > lucro:
                    lucro = cost+f[0].setup_cost - new_cost                  
                    swap[0] =  c.index
                    swap[1] =  fx[0].index 
        if force:      
            solution[swap[0]] = swap[1]
            TOTAL -= (lucro - f[0].setup_cost)       
        elif lucro > 0:                
            solution[swap[0]] = swap[1]
            TOTAL -= (lucro - f[0].setup_cost)     
                     
import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print 'Solving:', file_location
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)'

