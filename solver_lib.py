#!/usr/bin/python
# -*- coding: utf-8 -*-

import pdb
import math
from collections import namedtuple
from operator    import itemgetter
########################################################################
Point    = namedtuple("Point", ['x', 'y'])
Facility = namedtuple("Facility", ['index', 'setup_cost', 'capacity', 'location','s_per_c'])
Customer = namedtuple("Customer", ['index', 'demand', 'location'])

TOTAL    = 0
solution = [-1]*1
facilities = []
customers = []
customers_done = set()
facility_count = 0
customer_count = 0

#def objective_function():
#    obj = 0
#    for x in facilities:
#        if x.index in solution:
#          obj += x.setup_cost      
#    obj += TOTAL   
#    return obj
########################################################################
def length(point1, point2):
    return math.sqrt((point1.x - point2.x)**2 + (point1.y - point2.y)**2)
########################################################################
def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    global TOTAL
    global solution
    global facilities
    global facility_count
    global customer_count
    global customers
    # parse the input
    lines = input_data.split('\n')

    parts = lines[0].split()
    facility_count = int(parts[0])
    customer_count = int(parts[1])
    
    for i in range(1, facility_count+1):
        parts = lines[i].split()
        facilities.append(Facility(i-1, float(parts[0]), int(parts[1]), Point(float(parts[2]), float(parts[3])), float(parts[0]) / int(parts[1]) ))
                
    #customers = []
    for i in range(facility_count+1, facility_count+1+customer_count):
        parts = lines[i].split()
        customers.append(Customer(i-1-facility_count, int(parts[0]), Point(float(parts[1]), float(parts[2]))))
    
    #facilities = sorted(facilities, key = itemgetter(1))
    #facilities = sorted(facilities, key = itemgetter(4))
   
        
    solution = [-1]*len(customers)
    
    #customers = sorted(customers, key = itemgetter(1))
    #customers = sorted(customers, key = itemgetter(1), reverse=True)
    
    for customer in customers:
            solution[customer.index], X = minCost(customer,facilities,[])            
            TOTAL += X             
    for c in customers:
        #print c.index, solution[c.index]
        f = getFacility(solution[c.index])
        old = length(f.location,c.location)
        #print f.index,old
        #print_customers4facility(f.index)
        A, B = minCost(c,facilities,[]) 
        
        if B < old:
          print c.index, solution[c.index]
          print f.index,old
          print A,B
          #print A,B
          #print
          pdb.set_trace()
          break
        
    #done = False
    #for f in facilities:
    #   if done: break 
    #   c = max_customer4facility(f.index)
    #   old = length(f.location,c.location)
    #   print f.index,"\t", c.index,"\t",old 
    #   
    #   for x in facilities:
    #      new = length(x.location,c.location)
    #      if new < old:
    #          print "\t\t",new,"\tTREFFER:\t", x.index,"\t",x.capacity,"\t",facility_fillstate(x),"\t",c.demand 
    #          #pdb.set_trace()
    #          #solution[c.index] = x.index
    #          if (facility_fillstate(x) + c.demand) < x.capacity:
    #               #print solution 
    #               pdb.set_trace()
    #               solution[c.index] = x.index
    #               #print solution
    #               print_valuefunction()
    #               TOTAL -= old
    #               TOTAL += new
    #               print_valuefunction()
    #               #pdb.set_trace()
    #               done = True
    #               break
              
    ###
    used = [0]*len(facilities)
    for facility_index in solution:
        used[facility_index] = 1
        
    # calculate the cost of the solution
    obj = sum([f.setup_cost*used[f.index] for f in facilities])
    obj += TOTAL   
    
    #for x in facilities:
    #    if facility_fillstate(x) > x.capacity:
    #       print "ANALISE\t",x.index,"\t",x.capacity,"\t", facility_fillstate(x)
           
    print "Open Facilities", set(solution), " => ",len(set(solution))  
    print "Closed Facilities", set([x for x in range(50)]) - set(solution)  
    # prepare the solution in the specified output format
    output_data = str(obj) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))
    #print_valuefunction()
    
    #print_facilities()
    #print_customers4facility(42)
    #print_customers4facility(46)
    
    #print_facility_customer(42,74)
    #print_facility_customer(42,118)
    
    #print_facility_customer(46,11)
    #print_facility_customer(46,146)
    #print_facility_customer(46,193)
    #print
    #max_customer4facility(42)
    #print
    #max_customer4facility(46)
    
    #for f in facilities:
    #    if length(f.location,customers[11].location) < 20000:
    #      print "F.INDEX ",f.index, "\tF.CAPACITY ",f.capacity,"\tFILLSTATE \t", \
    #      facility_fillstate(f), "\tLENGTH 11 \t",length(f.location,customers[11].location)
     
    
    print           
    return output_data
########################################################################
def print_valuefunction():
    used = [0]*len(facilities)
    for facility_index in solution:
        used[facility_index] = 1
        
    # calculate the cost of the solution
    obj = sum([f.setup_cost*used[f.index] for f in facilities])
    obj += TOTAL    
    print "VALUEFUNCTION:", obj
########################################################################
def facility_fillstate(fac):  
       #global solution
       #global customers 
       assert  isinstance(fac,Facility),"fac must be an instance of Facility (1)"   
       s = 0
       for idx,x in enumerate(solution):
           if x == fac.index:
               s += customers[idx].demand
       return s       
########################################################################
def print_facilities():
    for x in facilities:
        print "Index:\t",x.index,"\tcapacity:\t",x.capacity,"\tused:\t", \
        facility_fillstate(x),"\t",x.capacity - facility_fillstate(x) 
########################################################################
def print_customers4facility(findex):
    fac = [ x for x in facilities if x.index==findex][0]
    assert  isinstance(fac,Facility),"fac must be an instance of Facility (2)" 
    print "Index:\t",findex,"\tcapacity:\t",fac.capacity, "\t",fac.setup_cost
    for idx,x in enumerate(solution):
        if x == findex:        
            print "\tcustomer: ",idx,"\tlength: ",length(fac.location,customers[idx].location), \
            "demand: ",customers[idx].demand        
########################################################################
def max_customer4facility(findex):
    fac = [ x for x in facilities if x.index==findex][0]
    assert  isinstance(fac,Facility),"fac must be an instance of Facility (3)" 
    #print "Index:\t",findex,"\tcapacity:\t",fac.capacity, "\t",fac.setup_cost
    m = 0
    c = 0
    for idx,x in enumerate(solution):
        if x == findex:      
            if m < length(fac.location,customers[idx].location):
               c = idx  
            m = length(fac.location,customers[idx].location) if m < length(fac.location,customers[idx].location) else m
            
    #print "facility\t",findex,"\tcustomer\t ",c,"\tmaxlength: ",m
            #"demand: ",customers[idx].demand        
    return customers[c]        
########################################################################            
#def print_facility_customer(findex,cindex):
#    fac = [ x for x in facilities if x.index==findex][0]
#    print "facility\t",fac.index,"\tcustomer\t",customers[cindex].index, \
#    "\tLength: ",length(fac.location,customers[cindex].location)
########################################################################
def minCost(customer,facilities,blacklist):
    global solution
    global customers
    m = 1000000
    
    i = -1
    for f in facilities:
        if f.index in facilities: continue
        if f.index in blacklist: continue
        l =  length(customer.location, f.location)   
        
        if (l < m)  and (facility_fillstate(f) + customer.demand <= f.capacity) :  
        #if (l < m)  :                 
            m = l
            i = f.index    
        
    return i , m               
######################################################################## 
def getFacility(findex):
    return [ x for x in facilities if x.index==findex][0]
########################################################################            
import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print 'Solving:', file_location
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)'
########################################################################
