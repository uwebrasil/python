import pdb
import math
from collections import namedtuple
from operator    import itemgetter
import random
import numpy as np
########################################################################
Point    = namedtuple("Point", ['x', 'y'])
Facility = namedtuple("Facility", ['index', 'setup_cost', 'capacity', 'location','s_per_c'])
Customer = namedtuple("Customer", ['index', 'demand', 'location'])
customers  = []
facilities = []
solution   = []
TOTAL = 0
M = None
########################################################################
def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    global customers, facilities, solution, M    
    
    lines = input_data.split('\n')
    
    parts = lines[0].split()
    facility_count = int(parts[0])
    customer_count = int(parts[1])
    solution = [-1]*customer_count
    
    for i in range(1, facility_count+1):
        parts = lines[i].split()
        facilities.append(Facility(i-1, float(parts[0]), int(parts[1]), Point(float(parts[2]), float(parts[3])), float(parts[0]) / int(parts[1]) ))
                
    for i in range(facility_count+1, facility_count+1+customer_count):
        parts = lines[i].split()
        customers.append(Customer(i-1-facility_count, int(parts[0]), Point(float(parts[1]), float(parts[2]))))     
           
    # 1000 zeilen( customers) a 100 spalten (facilities)
    M = np.zeros((customer_count,facility_count))
     
    for ci,c in enumerate(customers):                
        for fi,f in enumerate(facilities):
            M[ci][fi] = length(f.location, c.location)       
    
    solution = [-1]*len(customers) 
    fill_facility() 
    print objective_function()
    swapCustomerWithoutOpenNewFacility();       
    print objective_function()   
    #print openFacilities()
    #print closedFacilities()
        
    #print facilities[0]
    #print customers[100]          
    # prepare the solution in the specified output format
    obj = objective_function()
    output_data = str(obj) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))
       
    return output_data
########################################################################
def swapCustomerWithoutOpenNewFacility():
    global TOTAL, solution
    for idx,x in enumerate(solution):
        print idx, x, M[idx][x]
        fo = [a for a in set(solution) if not a == x]
        print fo
        maxliste = []
        for b in fo:
            a = M[:,b]
            mini = np.unravel_index(np.argmin(a), a.shape)
            maxliste.append( (mini[0],b,M[mini[0]][b] ) )
        print maxliste  
        print M[181][0]  
        
        # loop maxliste, add if ...
        TOTAL -= M[idx][x]
        ii = maxliste[0]
        TOTAL += M[ii[0]][ii[1]]        
        solution[idx] = solution[ii[1]] 
        solution[ii[1]] = x
        break
        
    
########################################################################
def objective_function():
    obj = 0
    for x in facilities:
        if x.index in solution:
          obj += x.setup_cost      
    obj += TOTAL   
    return obj
########################################################################
########################################################################
def length(point1, point2):
    return math.sqrt((point1.x - point2.x)**2 + (point1.y - point2.y)**2)
########################################################################
########################################################################            
def fill_facility():
    global TOTAL
    global solution
    used_customer = []
    for f in facilities:        
        used_capacity = 0   
        cl = clist(f,used_customer)  
        cl = sorted(cl, key = itemgetter(1))        
        if len(cl) == 0: break
        index = 0       
        while True:
           if used_capacity + cl[index][0].demand > f.capacity: break
           used_capacity += cl[index][0].demand
           solution[cl[index][0].index] = f.index 
           TOTAL += cl[index][1]
           used_customer.append(cl[index][0])
           index += 1
           if index >= len(cl): break
########################################################################
def clist(f,used_customer):
    cl = []
    for c in customers:
        if c in used_customer: continue
        cl.append( (c,length(f.location,c.location)) )
    cl = sorted(cl, key = itemgetter(1)) 
    return cl
########################################################################
########################################################################
def getFacility(findex):
    return [ x for x in facilities if x.index==findex][0]
########################################################################
def getCustomer(cindex):
    return [ x for x in customers if x.index==cindex][0]
########################################################################
def openFacilities():
    return list(set(solution))     
########################################################################
def closedFacilities():
    return [x.index for x in facilities if not x.index in openFacilities()]  
########################################################################
import sys
if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print 'Solving:', file_location
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)'
########################################################################
